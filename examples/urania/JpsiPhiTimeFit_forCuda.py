from Helicity import *
from Time import *
from os import system
from CppFunctionsMaker import FunctionPrinter

USE_MATHEMATICA = 1
Spins = [1]
A = doB2VX(Spins, helicities = [1,-1], transAng = 0)
pdf_split = DecomposeAmplitudes(A,TransAmplitudes.values())
phys = 0
integral = 0

def DefineWeakPhases(free_delta0 = 0):
    for key in TransAmplitudes:
        amp = str(TransAmplitudes[key])
        if key not in Lambdas.keys(): Lambdas[key] = One# USymbol("landa" + amp.replace("A",""), "\\|\\lambda\\|" + amp.replace("A",""),positive = True)
        if key not in WeakPhases.keys(): WeakPhases[key] = USymbol("phi"+ amp.replace("A",""), "\\Phi" + amp.replace("A",""),real = True)

free_delta0 = 0
DefineStrongPhases(free_delta0)
DefineWeakPhases(free_delta0)
#### Rotate CP = -1 phases (this is the way we handle \eta)
WeakPhases["0_0"] += Pi 
WeakPhases["1_pe"] += Pi

### change the free variables to cosines
x = USymbol("helcosthetaK", "c_K",real = True)
y = USymbol("helcosthetaL", "c_L",real = True)
z = USymbol("helphi", "\\phi",real = True)
CThL = Cos(ThetaL)
CThK = Cos(ThetaK)
def changeFreeVars(function):
    ### Not sure Phi is the same as in DTT , could be a sign flip
    function  = function.subs( Sin(2*ThetaK), 2*Sin(ThetaK)*Cos(ThetaK))
    function  = function.subs( Cos(2*ThetaK), 2*Cos(ThetaK)**2 - 1)
    function  = function.subs( Sin(ThetaK), Sqrt(1-Cos(ThetaK)**2))
    function  = function.subs( Sin(ThetaL), Sqrt(1-Cos(ThetaL)**2))
    function = function.subs([(CThK,x),(CThL,y), (Phi,z)])

    return function
  
TristanWeights = {}

CSP_factors = []
SpinMap = { v:k for k,v in TransAmplitudes.items()}

for key in pdf_split:
    if not pdf_split[key] : continue
    Amp1 = list(key.atoms())[0]
    Amp2 = list(key.atoms())[1]
    cte = One
    if Amp2 != 2 and Amp1 !=2:
        J1 = max( SpinMap[Amp1][0],  SpinMap[Amp2][0])
        J2 = min( SpinMap[Amp1][0],  SpinMap[Amp2][0])
        if J1 != J2 :
            cte = USymbol("Cfact_" + J1 + J2 , "C_{" + J1 + "," + J2 + "}", real = True)
            if cte not in CSP_factors: CSP_factors.append(cte)
    wname = "w_" + str(Amp1) + str(Amp2)
    wname = wname.replace("A_","")
    TristanWeights[key] =  float("2" in wname)  ### perfectly flat acceptance. Otherwise replace by a symbol.
    if pdf_split[key]:
        phys +=  timeExpression(key)* pdf_split[key]*cte
        integral += timeExpression(key) *  TristanWeights[key]*cte
     
    
phys = changeFreeVars(phys)

potential_list = [x,y,z,time]  +  TransAmpModuli.values() + TransAmpPhases.values() + WeakPhases.values() +[ Gamma , DG ,DM] + CSP_factors + TristanWeights.values() + Lambdas.values()
final_list = []

for thing in potential_list:
    if (thing in phys.atoms() or thing in integral.atoms()) and thing not in final_list: final_list.append(thing)

mysubs = (x**2, "x2"), (y**2,"y2"), (Exp(-Gamma*time), "exp_G_t"), (Cos(DM*time), "cdmt"), (Sin(DM*time),"sdmt"), (Cosh(DG*time/2), "cosh_term"),(Sinh(DG*time/2), "sinh_term")
eval = FunctionPrinter(phys, final_list,"evaluate")
eval.mysubs = mysubs
eval.make()

integral = simplify(integral)

inte = FunctionPrinter(integral, final_list,"int_over_Omega")
inte.mysubs = mysubs

inte.make()

if USE_MATHEMATICA : kappa = math_integrate(integral, (time,Symbol("t_0",positive = True), Symbol("t_1",positive = True)))
else: kappa = iter_integrate(integral, (time,Symbol("t_0",positive = True), Symbol("t_1",positive = True)))

inte2 = FunctionPrinter(kappa, final_list,"int_over_Omega_t")
inte2.mysubs = mysubs
inte2.make()

# Now let's calculate the derivatives. They can be useful if you write your own minimizer, or to get 'Probmax' for a toyMC generator

for thing in final_list:
    try: myf = simplify(phys.diff(thing))
    except ValueError: continue
    if not myf: continue
    der = FunctionPrinter(myf, final_list,"dphys_over_d" + str(thing))
    der.mysubs = mysubs
    der.make()





           
