#means = {'Y1': 9.48427e+03, 'Y2': 1.00466e+04, 'Y3': 1.03806e+04}
means =  {'Y1': 9460.30, 'Y2': 10023.26, 'Y3': 10355.2}
scales = {}
sigmas = {}
#lambdas = {'Y1':-1.15770e+00 , 'Y2': -1.19982e+00, 'Y3': -1.25112e+00 } 
lambdas = {}
zetas ={}
a1s  = {}
n1s = {}
a2s = {}
n2s = {}
#sigma0s = {}
betas = {}

def estimateSigma0(inputmass):
       slope = 0.00235
       y0 = 3.73
       sigma0 = inputmass*slope + y0
       return sigma0

for key in means.keys():
    #sigmas[key] = 9.13177e+01 ## Don't know why they are all equal
    sigmas[key] = 2.7 ## Don't know why they are all equal
    lambdas[key] = -1.3
    zetas[key] = 0.
    a1s[key] = 2.0
    n1s[key] = 1.0
    a2s[key] = 200.
    n2s[key] = 1.0
    scales[key] = 0.001
    betas[key] = -3.0e-03
    #sigma0s[key] = estimateSigma0(means[key])

P = {}

for key in means.keys():
       P[key] = {"s":sigmas[key], "l": lambdas[key], "beta":betas[key],"zeta": zetas[key], 'scale': scales[key], "m": means[key],
              "a1":a1s[key], "n1": n1s[key], "a2":a2s[key], "n2":n2s[key]}#, "s0":sigma0s[key]}

