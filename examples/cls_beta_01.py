from tools import GaussSmear1, plt
from timeit import default_timer as timer
import numpy as np
import pycuda.autoinit
import pycuda.cumath
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
import psIpatia as ipatia_module
import pycuda.gpuarray as gpuarray
from CLcalculator_beta1 import *
import matplotlib.pyplot as plt
from ROOT import TH1D, TFile
from data import datalist

## Get stuff, get types, etc 
NBINS = 10000 
sd = "float64" 
dtype = getattr(np,sd) 

## Set the mass range and binning:
masslow = np.arange(5179.,5379., 200./NBINS, dtype = dtype) #the array of lowedges 
DM = masslow[1] - masslow[0] ##step size 

massbins = np.append(masslow, np.array(max(masslow)+DM,dtype = dtype)) ##to make sure the last value is incorporated 
mass_center = masslow +0.5*DM ## array with bin centers 

## Load Ipatia:
mod = SourceModule(file("psIpatia.cu","r").read()) 

ipa = mod.get_function("Ipatia")

#### Ipatia parameters

mu = 5269
sigma0 = 5.
sigma = 6. 
beta = -1e-04 
l = -2 
a = 3 
n = 1 
a2 = 6 
n2 = 1 
k = -1e-03
Nb = 5000 

## The lines below are an example to convert unbinned data to histogram
### initialize some arrays in GPU

bins_gpu = gpuarray.to_gpu(mass_center)# cudriver.mem_alloc(mass_center.nbytes) 
bins_cx = bins_gpu.astype(np.complex128) 

tmp0 = np.zeros_like(mass_center)
sigf_i_gpu = gpuarray.to_gpu(tmp0)

#######################################
## Create array (==histogram) models  #
#######################################

ipa(bins_gpu, sigf_i_gpu, np.float64(mu),np.float64(sigma),np.float64(l), np.float64(beta), np.float64(a), np.float64(n), np.float64(a2),np.float64(n2) , block = (1000,1,1), grid = (max(len(tmp0)/1000,1),1)) ## sigf_i_gpu is being filled 

SM = GaussSmear1(bins_cx,sigma0) ### Create the smearing function for convolutions GaussSmear1(bins, sigma) 
sigf_gpu = SM.Smear(sigf_i_gpu) 
bkgf_gpu = pycuda.cumath.exp(np.float64(k)*bins_gpu) 

### Normalize

sigpdf_gpu = sigf_gpu/np.sum((sigf_gpu).get()) ## Binned, the *dm is not used 
bkgpdf_gpu = bkgf_gpu/np.sum((bkgf_gpu).get()) 

######################
## end array models ##
######################

def HistoPdf(ns, nb = Nb): return np.float64(ns) * sigpdf_gpu + np.float64(nb)*bkgpdf_gpu  ## Cooks the (si+bi) expectation arrays 


fake_data = gpuarray.to_gpu(np.uint32(tmp0))
generateBinned(fake_data, HistoPdf(ns = 70))

## CLs computation:
CL = CLcalculatorBasic()
CL.setData(fake_data)
CL.setExpected(HistoPdf)
start = timer()
a= CL.CLs(60, toys = 10000)
print "TOYS:", timer() - start
