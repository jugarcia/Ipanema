import pycuda.cumath
from timeit import default_timer as timer
from tools import plt
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
from math import pi
import pycuda.gpuarray as gpuarray
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import Parameter, Free, Cat
import numpy as np
from PhisModel import mod, myModel as Model
from phisParams import CSP

import cPickle
BLOCK_SIZE = 50 #(1 - 1024) For complex the max no. is smaller, I'm using 100 but didn't play much

integraB = mod.get_function("binnedTimeIntegralB")
integraBbar = mod.get_function("binnedTimeIntegralBbar")

def getGrid(thiscat, BLOCK_SIZE):
    Nbunch = thiscat.Nevts *1. / BLOCK_SIZE
    if Nbunch > int(Nbunch): Nbunch = int(Nbunch) +1
    else : Nbunch = int(Nbunch)
    return  (Nbunch,1,1)
   
cats, Params = [], []
from math import pi
Params.append(Free("fL",0.5, limits=(0.3,.6)))
Params.append(Free("fpe",0.2, limits=(0.,0.3)))
Params.append(Free("phis",0.8, limits=(-1.,1.)))
Params.append(Free("dpa",0., limits=(-pi,pi)))
Params.append(Free("dpe",0., limits=(-pi,pi)))

Params.append(Free("G",0.6603, limits=(0.6,0.7)))
Params.append(Free("DG",0.0805, limits=(0.06,0.1)))
Params.append(Free("DM",17.7, limits=(16.,20.)))
              
for i in range(1,7):
#for i in range(1,2):
    ibin = str(i)
    Bname = "B_"+ ibin
    Bbarname = "Bbar_"+ ibin
#    if i != 3: continue
    Params.append(Parameter("CSP_" + ibin ,CSP[i]))
    Params.append(Parameter("Fs_" + ibin,0, limits=(0.,.5)))
    Params.append(Parameter("ds_" + ibin,0., limits=(-10,10.)))

    ## Define category for i-th bin and flavour = B
    thiscat = Cat(Bname, "../phis/data_b_phi_0.5pi_"+ibin +".ext", getN = True)
    
    thiscat.integra = integraB
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)
    #continue
    ## Define category for i-th bin and flavour = Bbar
    thiscat = Cat(Bbarname, "../phis/data_bbar_phi_0.5pi_"+ibin +".ext", getN = True)

    thiscat.integra = integraBbar
    thiscat.bin = i
    thiscat.ibin = str(i)
    thiscat.block = (BLOCK_SIZE,1,1)
    thiscat.grid = getGrid(thiscat, BLOCK_SIZE)
    cats.append(thiscat)
     
manager = Model(Params, cats)
manager.createFit()

manager.fit.migrad()
manager.fit.hesse()
#manager.fit.minos()

#manager.createMultinest("mnest_party")
#manager.mnest_vals()
#manager.plotcat(cats[6])
#plt.show()
start = timer()
ary = manager.generate_cat(cats[6])
print "gen:", timer() - start

manager.plotcat_toy(cats[6],ary)
plt.show()
