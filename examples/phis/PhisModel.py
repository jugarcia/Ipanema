import numpy as np
import pycuda.autoinit
import pycuda.cumath
import pycuda.driver as cudriver
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray
from poisson_intervals import *
from toygen import poissonLL_b as poissonLL
from iminuit import Minuit
from ModelBricks import ParamBox, cuRead
from tools import getSumLL_large, getSumLL_short, plt, plot1D
from timeit import default_timer as timer
from phisParams import CSP

mod = cuRead("cuda_phis.c")
pdf = mod.get_function("TimeF")

_timebins = np.arange(0,14,14./10000)
_timebins_gpu = gpuarray.to_gpu(_timebins)
_pdf_time = _timebins_gpu.copy()*0

THR = 1e06
def mySum1(thing): return np.float64(sum(thing))
def mySum2(thing): return np.float64(sum(thing).get())


class myModel(ParamBox):
    def __init__(self, pars, cats):
        ParamBox.__init__(self, pars, cats)
        sizes = []
        for k in cats: sizes.append(k.Nevts)
        if max(sizes) > THR:
            self.getSumLL = getSumLL_large
            self.mySum = mySum2
        else:
            self.getSumLL = getSumLL_short
            self.mySum = mySum1
        self.pdf = pdf
        self.time_bins = _timebins_gpu
        self.pdf_time = _pdf_time
        self.N_tbins = self.time_bins.size

        
    def run_cat(self, cat,CSP, Fs,fL,fpe, dpa, dpe, dS, G, DG,DM,phis):  
        landa = np.float64(1.)
        As = np.sqrt(Fs)
        Fp = 1.-Fs
        A0 = np.sqrt(Fp*fL)
        Ape = np.sqrt(Fp*fpe)
        Apa = np.sqrt(Fp*(1-fL-fpe))
   
        self.pdf(cat.data,cat.Probs, A0, As, Apa, Ape, dpa, dpe, dS, CSP, G, DG, DM, landa, phis,cat.Nevts, block = cat.block,grid = cat.grid)
        cat.integra(self.time_bins,self.pdf_time, A0,As,Apa,Ape, G,DG,DM,phis, block = (1000,1,1),grid = (self.N_tbins/1000,1,1))
        norm = np.float64(cat.Nevts)*np.log(np.float64(np.sum(self.pdf_time.get())))
        return norm
    
    
    def __call__(self,*args):
        chi2 = np.float64(0.)
        N = self.dc
        phis = np.float64(args[N["phis"]])
        G = np.float64(args[N["G"]])
        DG = np.float64(args[N["DG"]])
        DM = np.float64(args[N["DM"]])
        fL = np.float64(args[N["fL"]])
        fpe = np.float64(args[N["fpe"]])
        dpa = np.float64(args[N["dpa"]])
        dpe = np.float64(args[N["dpe"]])
        #for key in N.keys():
         #   print key, args[N[key]]
        norm = np.float64(0.)

        for cat in self.cats:
            ibin = cat.ibin
            Fs = np.float64(args[N["Fs_" + ibin]])
            ds = np.float64(args[N["ds_" + ibin]])
            CSP = np.float64(args[N["CSP_" + ibin]])
            norm +=  self.run_cat(cat, CSP, Fs,fL,fpe, dpa, dpe, ds, G, DG,DM,phis)
            
        LL = map(self.getSumLL, self.cats)
        LLsum = self.mySum(LL)
        #print "LLsum:", LLsum
        return -2*LLsum + 2*norm

    def plotcat(self,cat, rebinning = 50, outfile='test_fit.pdf', yscale='log', interval = poisson_Linterval, plotf = plot1D):
        data = cat.data.get()[:,3]  ## only the decaytime
        bins = self.time_bins.get()
        dt = bins[1]-bins[0]
        bins2 = bins -np.float64(len(bins)*[-dt*0.5])
        bins2 = list(bins2)
        bins2.append(bins[-1]+ dt)
        bins2 = np.float64(bins2)
        datahist = np.histogram(data, bins2)[0]
        
        N = self.fit.values
        fL = np.float64(N["fL"])
        fpe = np.float64(N["fpe"])
        phis = np.float64(N["phis"])
        
        dpa = np.float64(N["dpa"])
        dpe = np.float64(N["dpe"])
       
        G = np.float64(N["G"])
        DG = np.float64(N["DG"])
        DM = np.float64(N["DM"])
        ibin = cat.ibin
        Fs = np.float64(N["Fs_" + ibin])
        dS = np.float64(N["ds_" + ibin])
        CSP = np.float64(N["CSP_" + ibin])
       
        norm =  self.run_cat(cat, CSP, Fs,fL,fpe, dpa, dpe, dS, G, DG,DM,phis)
        pdf_ = self.pdf_time.get()
        integral = sum(pdf_)#*dt
        pdf = (len(data)*1./integral)*pdf_
        
        fig , pltfit, pltpulls = plotf (datahist,pdf,bins,rebinning = rebinning, yscale = yscale, interval = interval)
        fig.tight_layout()
        fig.savefig(outfile)
