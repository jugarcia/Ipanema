from reikna import cluda
from reikna.fft import FFT, FFTShift
import pycuda.cumath
import pycuda.gpuarray as gpuarray
import numpy as np
import matplotlib.pyplot as plt
import pycuda.autoinit
from poisson_intervals import *
#BREAK
api = cluda.cuda_api()
dev = api.get_platforms()[0].get_devices()
#thr = api.Thread.create(dev)
thr = api.Thread.create([pycuda.autoinit.device])



class ConvolverFFT:
    def __init__(self, x):
        self.tmp = x.copy()
        self.out = x.copy()
        
        self.FFT =  FFT(x)
        self.FFTShift = FFTShift(x)
        self.fft = self.FFT.compile(thr)
        self.fftshift = self.FFTShift.compile(thr)
        
    def Convolve(self, a,b):
        Fa = a.copy()
        Fb = b.copy()
        self.fft(Fa,a)
        self.fft(Fb, b)
        
        self.fft(self.tmp, Fa*Fb, inverse = True)
        self.fftshift(self.out,self.tmp)
        return self.out.copy()
    

class SmearerFFT(ConvolverFFT):
    def __init__(self, smf):
        ConvolverFFT.__init__(self, smf)
        self.smf = smf
        self.Fb = self.tmp.copy()
        self.fft(self.Fb,self.tmp)
        
    def Smear(self, a):
        cineroso = a.astype(np.complex128)
        Fa = cineroso.copy()
        self.fft(Fa,cineroso)
        
        self.fft(self.tmp, Fa*self.Fb, inverse = True)
        self.fftshift(self.out,self.tmp)
        return self.out.real.copy()
    
        
class GaussSmear1(SmearerFFT):
    def __init__(self, bins_gpu, sigma):
        M = gpuarray.take(bins_gpu,gpuarray.to_gpu(np.int32([int(bins_gpu.size*0.5)]))).get()[0]
        d = bins_gpu - M
        a = np.float64(0.5/(sigma*sigma))
        self.sigma = sigma
        f = pycuda.cumath.exp(-a*d*d)
        SmearerFFT.__init__(self, f/np.sum(f.get()))


def getSumLL_large(cat): return gpuarray.sum(pycuda.cumath.log(getattr(cat,"Probs")))
def getSumLL_short(cat): return np.sum(pycuda.cumath.log(getattr(cat,"Probs")).get())

def get_pulls(data,pdf, interval):
    errl = []
    errh = []
    for k in data:
        l, h = interval(k)
        errl.append(l)
        errh.append(h)
    errl = np.array(errl)
    errh = np.array(errh)
    deltas = data-pdf
    return np.array([(deltas[i]/errl[i] if deltas[i]>0  else deltas[i]/errh[i]) for i in range(np.size(deltas))]), errl, errh

def plot1D(data, pdf, bins, rebinning = 32,yscale = '', interval = poisson_Linterval):
    assert np.size(bins)%rebinning==0
    bins_rebin = np.sum(bins[i::rebinning] for i in range(rebinning))/rebinning #averaging
    pdf_rebin = np.sum(pdf[i::rebinning] for i in range(rebinning)) #summing
    data_rebin = np.sum(data[i::rebinning] for i in range(rebinning)) #summing
    pulls,errl, errh = get_pulls(data_rebin, pdf_rebin, interval)

     ## Now plot the result
    fig, (pltfit, pltpulls) = plt.subplots(2,1,figsize=(10, 7), sharex=True, gridspec_kw = {'height_ratios':[5, 1]})

    pltfit.errorbar(bins_rebin, data_rebin, yerr=[errl, errh], fmt='.', color='k') 
    pltfit.plot(bins, pdf*rebinning, '-', color='blue', linewidth=1)

    if yscale=='log': pltfit.set_yscale('log')
    pltpulls.errorbar(bins_rebin, pulls, yerr=[np.ones(len(errl)), np.ones(len(errh))], fmt='.', color='k') 
    pltpulls.plot(bins_rebin, pulls, marker='.', linestyle='-', color='k')

    pltpulls.set_ylim(-5, 5)
    pltpulls.set_yticks([-5, -3, 0, +3, +5])
    pltpulls.yaxis.grid(True, linestyle='--', which='major', color='grey')
    pltpulls.set_xlim(bins[0], bins[-1])
    #pltpulls.set_xlabel('$m(\mu\mu)$ MeV/$c^2$', fontsize=20)

    return fig, pltfit, pltpulls
