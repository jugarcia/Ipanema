import numpy as np
import pycuda.gpuarray as gpuarray
from timeit import default_timer as timer
import rotate
import cPickle
import toygen
from pycuda.compiler import SourceModule
rot = SourceModule(rotate.cu)
from os import system as shell
matrixXvector = rot.get_function("transform_f32")
#from multiprocessing import Pool
#pool = Pool( processes = 24)
from iminuit import *
import pymultinest as mnest

def cuRead(thing, **kwargs): return SourceModule(file(thing,"r").read(), **kwargs)
def getName(par): return par.name
class Parameter:
    def __init__(self, name, var = 0, limits = (), stepsize = 0, constant = True, dtype = np.float64):
        self.name = name
        self.dtype = dtype
        self.setVal(var)
        self.limits = limits
        self.constant = constant
        if limits: self.autoStepSize()
        else: self.stepsize = stepsize
        if stepsize: self.stepsize = stepsize
    def setVal(self, var):
        self.default = self.dtype(var)
        self.fit_init = self.dtype(var)
        self.val = self.dtype(var)
        
    def autoStepSize(self):
        self.stepsize = abs(self.limits[1]-self.limits[0])*1./10
    def setLimits(self,m,M, constant = False):
        if m > M:
            print self.name, " Warning: Upper Bound lower than lower bound. Reverting"
            m_ = M*1.
            M_ = m*1.
            m = m_
            M = M_
        self.limits = (m,M)
        self.constant = constant
        if m > self.fit_init or M < self.fit_init:
            print self.name, ": Init value not inside Boundaries, setting to ", 0.5*(M-m)
            self.fit_init = 0.5*(M-m)
        self.autoStepSize()
        
    def getSettings(self):
        out = {self.name: self.fit_init}
        if self.limits: out .update ({"limit_" + self.name: self.limits})
        if self.stepsize: out.update({"error_" + self.name: self.stepsize})
        if self.constant: out .update({"fix_" + self.name: True})
        return out

class Cat:
    def __init__(self, name, ary= [], Probs_ary = [], getN = False, N = 0):
        self.name = name
        if N: self.bookProbs(N)#self.Probs = gpuarray.to_gpu(np.float64(N*[0.]))
        if ary != []: self.setData(ary, getN)
            
    def setData(self, ary, getN = False):
        if isinstance(ary, np.ndarray):
            self.np_data = ary
            self.data = gpuarray.to_gpu(ary)
            if getN: self.bookProbs(len(ary))
        elif isinstance(ary, gpuarray.GPUArray):
            if getN:
                print "Warning: Number of events set to GPUArray size. This may be a bad idea if you are not in 1D"
                self.bookProbs(ary.size)
            self.data = ary.copy()
        elif isinstance(ary, list):
            ar = np.float64(ary)
            self.setData(ar, getN)
        elif isinstance(ary, file):
            ar = cPickle.load(ary)
            self.setData(ar, getN)
        elif isinstance(ary, str):
            f = file(ary)
            self.setData(f, getN)

        else: "dunno"
    def bookProbs(self,N):
        self.Nevts = np.int32(N)
        self.Probs = gpuarray.to_gpu(np.float64(N*[0.]))
        

class ParamBox:
    def __init__(self, params, cats = []):
        self.params = params
        self.cats = cats
        self.func_code = Struct(
            co_varnames = map(getName, self.params),
            co_argcount = len(self.params)
            )
        self.dc = {}
        self.Params = {}
        for i in range(len(self.params)):
            self.dc[self.params[i].name] = i
            self.Params[self.params[i].name] = self.params[i]

    def freeThese(self, pars):
        for par in pars: self.Params[par].constant = False
    def lock_to_init(self, pars):
        for par in pars: self.Params[par].constant = True


    #def constrain(self, name, m, s):
    def createFit(self):
        config = {}
        for par in self.params: config.update(par.getSettings())
        self.fit = Minuit(self, **config)
    def createMultinestInputs(self):
        def prior(cube,ndim,nparams):
            for i in xrange (len(self.params)):
                par = self.params[i]
                if par.constant: cube[i] = par.val
                else: cube[i] = par.limits[0] + (par.limits[1]-par.limits[0])*cube[i]
                #print "Prior param ",i, par.name, cube[i]
        self.mnest_prior = prior
        self.hypercube = np.float64(len(self.params)*[0.])
        def mLL(cube,ndim,nparams,lnew):
            #print "crap:", ndim, nparams, lnew
            for i in xrange(len(self.params)): self.hypercube[i] = cube[i]
            return -0.5*self(*(self.hypercube))
        self.mnest_LL = mLL
    def createMultinest(self, savedir,reset = False, **kwargs):
        self.createMultinestInputs()
        shell ("mkdir " +savedir)
        if reset: shell("rm " + savedir + "/*")
        npar = len(self.params)
        mnest.run(self.mnest_LL, self.mnest_prior,npar, outputfiles_basename= savedir + "/1-", **kwargs)
        self.readMultinest(savedir)
        
    def readMultinest(self, savedir):
        self.mnest_ana =  mnest.analyse.Analyzer(len(self.params), outputfiles_basename= savedir + "/1-")
        def sigmas():
            a = self.mnest_ana.get_mode_stats()
            v = a[u'modes'][0][u'mean']
            s = a[u'modes'][0][u'sigma']
            dc = {}
            for i in xrange(len(self.params)):
                par = self.params[i]
                dc[par.name] = [v[i],s[i]]
                print par.name, v[i],"\\pm", s[i]
            return dc
        self.mnest_vals = sigmas
        self.margplot = mnest.PlotMarginalModes(self.mnest_ana)
        def plot_marginal(*args):
            n = []
            for st in args: n.append(self.dc[st])
            self.margplot.plot_marginal(*n)
        self.plot_marginal = plot_marginal
        
    def fitSummary(self): return FitSummary(self.fit)
    def saveFitSummary(self, name):
        c = self.fitSummary()
        c.save(name)

    
class Free(Parameter):
    def __init__(self, name, var = 0, limits = (), stepsize = 0, dtype = np.float64): Parameter.__init__(self, name, var = var, limits = limits, stepsize = stepsize, constant = False, dtype = dtype)

class FitSummary:
    def __init__(self, fit):
        self.values = fit.values
        self.errors = fit.errors
        self.C = np.matrix(fit.matrix())
        self.cinv = np.matrix(np.linalg.inv(self.C))
        
        #self.var2pos = fit.var2pos
        #self.pos2var = fit.pos2var
        self.free = fit.list_of_vary_param()
        self.func_code = Struct(
            co_varnames = self.free,
            co_argcount = len(self.free)
            )
        self.table = {}
        for i in range(len(self.free)): self.table[self.free[i]] = i
        eL, eV = np.linalg.eig(self.cinv)
        self.R = np.matrix(eV)
        self.Ri = np.matrix(np.linalg.inv(self.R))
        self.eL = np.matrix(np.diag(eL))
        self.S = np.sqrt(self.eL)
        self.Si = np.linalg.inv(self.S)
        self.T = self.R*self.Si
        self.mean = np.matrix(map(self.values.get, self.free))
        self.T_gpu = gpuarray.to_gpu(np.float32(self.T))
        self.nvars = np.int32(len(self.free))

        
    def save(self, fname): cPickle.dump(self,file(fname, "w"))
    #def mu(self, var): return self.values[var]
    def sigma(self,var): return self.errors[var]
    
    def chi2(self,point):
        Y = np.matrix(map(point.get, self.free))
        d = Y - self.mean
        print d*self.cinv*d.transpose()
    
    def __call__(self, *args):
        Y = np.matrix(args)
        d = Y - self.mean
        return (d*self.cinv*d.transpose())[0][0] 
    
    def createFit(self): self.fit = Minuit(self)
    
    def rotate(self, ary):
        return self.T*np.matrix(ary).transpose()
    
    def generate(self, N, dtype = 'float32'):
        N2 = int(N*self.nvars)
        l = np.zeros( N2, dtype)
        ary = gpuarray.to_gpu(l)
        self.deltas_gpu = ary.copy()
        toygen.rand.fill_normal(ary)
        
        matrixXvector(ary,self.deltas_gpu,self.T_gpu,self.nvars, block=(1,1,1), grid = (int(N),1,1))
        self.deltas = self.deltas_gpu.get()

    def pickGenerated(self, i):
        dc = {}
        N = self.nvars
        i0 = i*N
        for i in xrange(N):
            key = self.free[i]
            dc[key] = self.deltas[i0+i] + self.values[key]
        return dc
        
        
