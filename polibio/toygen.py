import toyrand
import numpy as np
import pycuda.autoinit
import pycuda.gpuarray as gpuarray

rand = toyrand.XORWOWRandomNumberGenerator()
poisson_gpu = rand.module.get_function("poisson_int")
## normal_gpu = rand.module.get_function("normal_double")
## normal_gpu_f32 = rand.module.get_function("normal_float")

def poissonLL(dat_gpu,poiss_m): return np.float64(gpuarray.sum(dat_gpu*pycuda.cumath.log(poiss_m) -(poiss_m) ).get()) ## The .get() at the end adds an overhead, but it is only ~2percent of the total function time budget. Most of the shit is bcs of .sum

def poissonLL_b(dat_gpu,poiss_m):
    return np.sum((dat_gpu*pycuda.cumath.log(poiss_m) -(poiss_m) ).get())

def poissonLL_mask(dat_gpu,poiss_m, mask):
    LL = dat_gpu*pycuda.cumath.log(poiss_m) -(poiss_m)
    xLL = LL*mask
    return np.sum(xLL.get())

def generateBinned(out, model):
    poisson_gpu(rand.state, out, model, np.int64(out.size), block = (512,1,1), grid = (out.size/512,1))
